<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use Auth;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        // $posts = DB::table('pertanyaan')->get();
        $posts = Post::all();
        // dd($posts);
        return view ('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view ('posts.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required',
        ]);

        $post = Post::create([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'user_id' => Auth::id()
        ]);

        // $post = new Post;
        // $post->judul = $request['judul'];
        // $post->isi = $request['isi'];
        // $post->save();

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($post);
        $post = Post::find($id);
        return view ('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = Post::find($id);
        return view ('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required',
        ]);

        // $query = DB::table('pertanyaan')
        //         ->where('id',$id)
        //         ->update([
        //             "judul" => $request["judul"],
        //             "isi" => $request["isi"]
        // ]);

        $update = Post::where('id', $id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
        return redirect('/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Post::destroy($id);
        return redirect('/posts');
    }
}
