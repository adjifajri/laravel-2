<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use App\Http\Controllers\PertanyaanController;

Route::get('/', 'HomeController@index');
Route::get('/datatable', 'PertanyaanController@dataTable');

// Route::get('/posts/create', 'PertanyaanController@create');
// Route::post('/posts', 'PertanyaanController@store');
// Route::get('/posts', 'PertanyaanController@index');
// Route::get('/posts/{id}', 'PertanyaanController@show');
// Route::get('/posts/{id}/edit', 'PertanyaanController@edit');
// Route::put('/posts/{id}', 'PertanyaanController@update');
// Route::delete('/posts/{id}', 'PertanyaanController@destroy');

Route::resource('posts', 'PertanyaanController');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
// Route::get('/', 'PertanyaanController@index');
