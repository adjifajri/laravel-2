@extends('template.master')

@section('title')
    Post Create
@endsection

{{-- @section('header')

@endsection --}}

@section('content')


    <section class="content">


<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">@yield('title')</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/posts" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul Pertanyaan</label>
        <input type="text" class="form-control" id="judul" value="{{old('judul')}}" name="judul" placeholder="Isi Judul">
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
         @enderror
        </div>

        <div class="form-group">
          <label for="isi">Isi Pertanyaan</label>
          <input type="text" class="form-control" id="isi" name="isi" placeholder="Isi Pertanyaan">
          @error('isi')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
      </div>
        <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
</section>
  </div>


@endsection
