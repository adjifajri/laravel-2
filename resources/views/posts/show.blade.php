@extends('template.master')

@section('title')
    Show Data
@endsection

@section('content')

<div class="mt-3 mt-3">

    <section class="content">

        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">@yield('title')</h3>
            </div>
        <div class="mt-3 ml-3">
            <div class="card">
                <div class="card-header">


                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        {{-- <th>No</th> --}}
                        <th>Judul</th>
                        <th>Isi Pertanyaan</th>
                        <th style="width: 40px">Action</th>
                      </tr>
                    </thead>
                    <tbody>

            <tr>
                {{-- <td>{{ $post -> id }} </td> --}}
                <td>  {{ $post -> judul }} </td>
                <td>  {{ $post -> isi  }} </td>
            <td> <a href="/posts" <button type="submit" class="btn btn-primary">Back</button></a>  </td>

            </tr>

                    </tbody>
                  </table>
                </div>
            </div>

        </div>
        </div>
        </div>



</div>

@endsection
