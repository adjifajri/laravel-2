@extends('template.master')

@section('title')
    Post
@endsection

{{-- @section('header')

@endsection --}}

@section('content')

<section class="content">

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">@yield('title')</h3>
    </div>
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">

          <a href="{{url('posts/create')}}" <button type="submit" class="btn btn-primary">Buat Pertanyaan</button></a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">No</th>
                <th>User</th>
                <th>Judul</th>
                <th>Isi Pertanyaan</th>
                <th>Di Buat Tanggal</th>
                <th>Update</th>
                <th style="width: 40px">Action</th>
              </tr>
            </thead>
            <tbody>
@foreach ($posts as $post)
    <tr>
        <th scope="row"> {{ $loop->iteration }}</th>
        <td>  {{ $post -> user_id }} </td>
        <td>  {{ $post -> judul }} </td>
        <td>  {{ $post -> isi  }} </td>
        <td>  {{ $post -> created_at  }} </td>
        <td>  {{ $post -> updated_at  }} </td>
    <td style="display: flex">
        <a href="/posts/{{$post->id}}" <button type="submit" class="btn btn-primary">Show</button></a>
        <a href="/posts/{{$post->id}}/edit" <button type="submit" class="btn btn-warning">Edit</button></a>
    <form action="/posts/{{$post->id}}" method="post">
        @method('delete')
        @csrf
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
    </td>
    </tr>
@endforeach
            </tbody>
          </table>
        </div>
    </div>

</div>
</div>
</div>


@endsection
